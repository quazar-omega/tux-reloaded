# Tux Reloaded

A Tux redesign in honor of Linux

<div align="center">
  <img alt="Tux Linux logo" src="./illustration/color/combination/Tux-Linux-flattened.svg" width="250px">
</div>

- Be free to choose
- Own your technology
- Contribute to the community as a selfless act

It's all about *U* 💛

## Acknowledgements
Fonts:
- [Urbanist](https://github.com/coreyhu/Urbanist)
- [Yeseva One](https://fonts.google.com/specimen/Yeseva+One) (maybe this is the source? https://github.com/alexeiva/yesevaone)

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
	<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Tux Reloaded</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://codeberg.org/quazar-omega/tux-reloaded" property="cc:attributionName" rel="cc:attributionURL">QuazarOmega</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
<br />
Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://isc.tamu.edu/~lewing/linux/" rel="dct:source">https://isc.tamu.edu/~lewing/linux/</a>.
